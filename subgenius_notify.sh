#!/usr/bin/env bash

# Copyright ГК Азъ

# окно ввода значения времени показа текста

time=$(zenity 2>/dev/null --title  "Введите значение"  --entry --text="Введите время (t>0) показа сообщения, сек. (по умолчанию 5)" --width 480 --height 200)

# проверяем поле ввода времени показа сообщения на соответствие
# условие: это должно быть число больше или равно 1
# иначе значение останется по умолчанию

if [ $(echo "$time" | grep -qE '^[1-9]+$'; echo $?) -ne "0" ]; then
    time=5
    echo "Время показа сообщения оставили по умолчанию, сек.:" $time
else
    time=$time
    echo "Выбрано время показа сообщения, сек.:" $time
fi


# окно ввода значения времени задержки между показом сообщений

pause=$(zenity 2>/dev/null --title  "Введите значение"  --entry --text="Введите время задержки (t>=0) между показом сообщений, сек. (по умолчанию 2)" --width 480 --height 200)

# проверяем поле ввода времени задержки между показом сообщений
# условие: это число должно быть число больше или равно 0
# иначе значение останется по умолчанию

if [ $(echo "$pause" | grep -qE '^[0-9]+$'; echo $?) -ne "0" ]; then
    pause=2
    echo "Время задержки между показом сообщений оставили по умолчанию, сек.:" $pause
else
    pause=$pause
    echo "Выбрано время задержки между показом сообщений, сек.:" $pause
fi

# окно ввода названия программы для показа логотипа

label=$(zenity 2>/dev/null --title  "Введите название"  --entry --text="Введите название программы для показа логотипа" --width 480 --height 200)

# проверяем поле ввода названия программы для показа логотипа

if [ -z "$label" ]; then 
    label=help
    echo "Оставлен значок по умолчанию:" $label
else
    label=$label
    echo "Выбран значок:" $label
fi

# окно выбора текстового файла с подсказками

in_file_name=$(zenity 2> /dev/null --file-selection --title  "Выберите файл")

# если файл не выбран - программа закрывается
# если выбран - начинается показ текста в терминале

if [ -z "$in_file_name" ]; then 
    echo "Не выбран файл, выходим"
    exit 0;
else
    echo "Выбран файл: $in_file_name, начинаем работу"
fi

koeff=1000
let times=$time*$koeff
let slp=$time+$pause
notifys=$(echo "sleep $slp; notify-send -t $times -i $label")

main() {
    local line_num=1

    cat "$in_file_name" | while read line; do
        txt=$(cat $in_file_name | sed -n ${line_num}p)
        echo "$notifys \"$line_num\" \"$txt\""
        let line_num++
    done
}

main $* > /tmp/ns.sh
chmod +x /tmp/ns.sh
/tmp/ns.sh
