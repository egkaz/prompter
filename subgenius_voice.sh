#!/usr/bin/env bash

# Copyright ГК Азъ

# окно выбора диктора
prompter=$(zenity 2>/dev/null --list \
--title="Выбор диктора" \
--text "Выберите диктора из списка" \
--radiolist \
--column "" \
--column "" \
TRUE "Anna" \
FALSE "Elena" \
FALSE "Irina" \
FALSE "Aleksandr")

if [ -z "$prompter" ]; then 
    prompter=Anna
    echo "Диктор оставлен по умолчанию:" $prompter
else
    prompter=$prompter
    echo "Выбран диктор:" $prompter
fi

# окно выбора англоговорящего диктора
prompter2=$(zenity 2>/dev/null --list \
--title="Выбор англ. диктора" \
--text "Выберите англ. диктора из списка" \
--radiolist \
--column "" \
--column "" \
TRUE "None" \
FALSE "Slt" \
FALSE "Clb" \
FALSE "Alan")

if [ -z "$prompter2" ]; then 
    prompter2="None"
    echo "Англ. диктор отсутствует"
else
    prompter2=$prompter2
    echo "Выбран англ. диктор:" $prompter2
fi

# окно выбора скорости чтения (по стандарту - от -1 до 1 с шагом 0.1)
speed=$(zenity 2>/dev/null --scale --title="Скорость чтения" --text="Выберите скорость чтения" --min-value=-9 --max-value=9 --value=0 --step=1)

# если не выбрано значение - оно останется по умолчанию (0)
# поэтому далее выбранное значение необходимо умножить на 0.1
if [ -z "$speed" ]; then 
    speed=0
    echo "Скорость чтения текста оставили по умолчанию:" $speed
else
    speed=$speed
    echo "Выбрана скорость чтения текста:" $speed
fi

# окно выбора высоты голоса, по стандарту - от -1 до 1 с шагом 0.1
# поэтому далее выбранное значение необходимо умножить на 0.1
tone=$(zenity 2>/dev/null --scale --title="Высота голоса" --text="Выберите высоту голоса речи" --min-value=-9 --max-value=9 --value=0 --step=1)

# если не выбрано значение - оно останется по умолчанию (0)
if [ -z "$tone" ]; then 
    tone=0
    echo "Высоту голоса речи оставили по умолчанию:" $tone
else
    tone=$tone
    echo "Выбрана высота голоса речи:" $tone
fi

# окно ввода значения времени задержки между чтением строк
pause=$(zenity 2>/dev/null --title  "Введите значение"  --entry --text="Введите время задержки (t>=0) между чтением строк, сек. (по умолчанию 2)" --width 480 --height 200)

# проверяем поле ввода времени задержки между чтением строк
# условие: это число должно быть число больше или равно 0
# иначе значение останется по умолчанию
if [ $(echo "$pause" | grep -qE '^[0-9]+$'; echo $?) -ne "0" ]; then
    pause=2
    echo "Время задержки между чтением строк оставили по умолчанию, сек.:" $pause
else
    pause=$pause
    echo "Выбрано время задержки между чтением строк, сек.:" $pause
fi

# окно выбора текстового файла для диктовки
in_file_name=$(zenity 2> /dev/null --file-selection --title  "Выберите файл")

# если файл не выбран - программа закрывается
# если выбран - начинается показ текста в терминале
if [ -z "$in_file_name" ]; then 
    echo "Не выбран файл, выходим"
    exit 0;
else
    echo "Выбран файл: $in_file_name, начинаем работу"
fi

# коэффициент для дальнейших операций с положительными и отрицательными значениями
koeff=-1

# проверяем введённое значение скорости речи и приводим его в нужное состояние
if [ $speed -lt 0 ]; then
let speeds=$koeff*$speed
else
speeds=$speed
fi

if [ $speed -lt 0 ]; then
print_speed="-0.$speeds"
else print_speed="0.$speeds"
fi

# проверяем введённое значение тона и приводим его в нужное состояние
if [ $tone -lt 0 ]; then
let tones=$koeff*$tone
else
tones=$tone
fi

if [ $tone -lt 0 ]; then
print_tone="-0.$tones"
else print_tone="0.$tones"
fi

# начало обработки
main() {
    local line_num=1
    echo "#!/usr/bin/env bash"

# если необходимо очищать терминал перед началом работы - раскомментируйте следующую строку
#     echo "sleep $pause; reset"

    cat "$in_file_name" | while read line; do
        echo "sleep $pause; sed -n ${line_num}p '$in_file_name' | RHVoice-client -s $prompter+$prompter2 -r $print_speed -p $print_tone | aplay"
        let line_num++
    done
}

# создаём временный файл в /tmp
# если необходимо удалять его после выполнения скрипта - раскомментируйте последнюю строку
main $* > /tmp/voi.sh
chmod +x /tmp/voi.sh
/tmp/voi.sh
#rm /tmp/voi.sh